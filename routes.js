module.exports = {
    routes: {
	'assets://storyfiles/hs2/04097/04097_original.swf': './swf/04097_original.swf', // asset link is pseudo-path, see <(Redirect page 006000 flash to start of Jailbreak)>
	/// Character Select Screens
	// Page 006021
	'assets://storyfiles/hs2/04118/04118_unfinished1.swf': './swf/04118_unfinished1.swf',
	'assets://storyfiles/hs2/04118/04118_unfinished2.swf': './swf/04118_unfinished2.swf',
	// Page 006062
	'assets://storyfiles/hs2/04159/04159_unfinished2.swf': './swf/04159_unfinished2.swf',
	// Page 006369
	'assets://storyfiles/hs2/04466/04466_unfinished1.swf': './swf/04466_unfinished1.swf',
	'assets://storyfiles/hs2/04466/04466_unfinished2.swf': './swf/04466_unfinished2.swf',
	// Page 006416
    'assets://storyfiles/hs2/04513/04513_unfinished2.swf': './swf/04513_unfinished2.swf',
	// Page 006934
	'assets://storyfiles/hs2/05031/05031_unfinished1.swf': './swf/05031_unfinished1.swf',
	'assets://storyfiles/hs2/05031/05031_unfinished2.swf': './swf/05031_unfinished2.swf',
	'assets://storyfiles/hs2/05031/05031_unfinished3.swf': './swf/05031_unfinished3.swf',
	'assets://storyfiles/hs2/05031/05031_unfinished4.swf': './swf/05031_unfinished4.swf',
	// Page 006952
	'assets://storyfiles/hs2/05049/05049_unfinished2.swf': './swf/05031_unfinished2.swf', // identical file
	'assets://storyfiles/hs2/05049/05049_unfinished3.swf': './swf/05031_unfinished3.swf', // identical file
	'assets://storyfiles/hs2/05049/05049_unfinished4.swf': './swf/05031_unfinished4.swf', // identical file
	// Page 006970
	'assets://storyfiles/hs2/05067/05067_unfinished3.swf': './swf/05031_unfinished3.swf', // identical file
	'assets://storyfiles/hs2/05067/05067_unfinished4.swf': './swf/05031_unfinished4.swf', // identical file
	// Page 006978
	'assets://storyfiles/hs2/05075/05075_unfinished4.swf': './swf/05031_unfinished3.swf', // identical file
	// Page 007037
	'assets://storyfiles/hs2/05134/05134_unfinished1.swf': './swf/05134_unfinished1.swf',
	'assets://storyfiles/hs2/05134/05134_unfinished2.swf': './swf/05134_unfinished2.swf',
	'assets://storyfiles/hs2/05134/05134_unfinished3.swf': './swf/05134_unfinished3.swf',
	'assets://storyfiles/hs2/05134/05134_unfinished4.swf': './swf/05134_unfinished4.swf',
	// Page 008199
	'assets://storyfiles/hs2/26297/26297_unfinished1.swf': './swf/26297_unfinished1.swf',
	'assets://storyfiles/hs2/26297/26297_unfinished2.swf': './swf/26297_unfinished2.swf',
	'assets://storyfiles/hs2/26297/26297_unfinished3.swf': './swf/26297_unfinished3.swf',
	// Page 009386
	'assets://storyfiles/hs2/07482/07482_unfinished2.swf': './swf/07482_unfinished2.swf',
	'assets://storyfiles/hs2/07482/07482_unfinished3.swf': './swf/07482_unfinished3.swf',
	'assets://storyfiles/hs2/07482/07482_unfinished4.swf': './swf/07482_unfinished4.swf',
	'assets://storyfiles/hs2/07482/07482_unfinished5.swf': './swf/07482_unfinished5.swf',
	'assets://storyfiles/hs2/07482/07482_unfinished6.swf': './swf/07482_unfinished6.swf',
	'assets://storyfiles/hs2/07482/07482_unfinished7.swf': './swf/07482_unfinished7.swf',
	'assets://storyfiles/hs2/07482/07482_unfinished8.swf': './swf/07482_unfinished8.swf'
    }
}
