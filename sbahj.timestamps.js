module.exports = {
    sbahjPageIsSpoiler: function (page) {
	// Timestamps from image metadata, for the most part
	const page_timestamp = {sbahj1: 1237363854,
				sbahj2: 1237335262,
				sbahj3: 1237394564,
				sbahj4: 1245474457,
				sbahj5: 1249170639,
				sbahj6: 1249966211,
				sbahj7: 1256270259,
				sbahj8: 1259459961,
				sbahj9: 1260921600,
				sbahj10: 1261713848,
				sbahj11: 1263086587,
				sbahj12: 1266995456,
				sbahj13: 1267598113,
				sbahj14: 1268371980,
				sbahj15: 1269066842,
				sbahj16: 1272697965,
				sbahjmovie1: 1274680800, // First capture in web archive, only used for linkhole
				sbahj17: 1277794529,
				sbahj18: 1285665901,
				sbahj19: 1287977389,
				sbahj20: 1289702467,
				sbahj21: 1289702467, // no metadata, related to above, copy above
				sbahj22: 1289702126,
				sbahj23: 1293432332,
				sbahj24: 1294655301,
				sbahj25: 1296371005,
				sbahj26: 1298769873,
				sbahj27: 1302406923,
				sbahj28: 1303451132,
				sbahj29: 1303451132, // no metadata, released alongside above
				sbahj30: 1303453684,
				sbahj31: 1305188046,
				sbahj32: 1307257759,
				sbahj33: 1307298195,
				sbahj34: 1308542400, // no metadata, unrelated to above
				                     // came out Monday according to https://tvtropes.org/pmwiki/pmwiki.php/Trivia/SweetBroAndHellaJeff
				                     // 4/20 joke, the Monday before appearance in web archive is 6/20, so using 6/20
				sbahj35: 1312444902,
				sbahj36: 1316925238,
				sbahj37: 1320519243,
				sbahj38: 1323764033,
				sbahj40: 1324191341,
				sbahj41: 1326335558,
				sbahj42: 1328163625,
				sbahj43: 1334387535,
				sbahj44: 1335158588,
				sbahj45: 1380088926,
				sbahj46: 1380947045,
				sbahj47: 1382938929,
				sbahj48: 1382940200,
				sbahj49: 1386045480, // no metadata, timestamp taken from Reddit post (granularity 1 minute for no reason)
				sbahj50: 1386477910,
				sbahj51: 1412798287,
				sbahj52: 1412909170,
				sbahj53: 1510722720, // Large gap in time between metadata and Reddit post, using Reddit post
				sbahj54: 1510722720  // Released alongside above
			       }
	if (page in page_timestamp) {
	    let timeNow = Math.floor(new Date().getTime()/1000.0)
	    let fakeTimeNow = timeNow - secondsFromThenToNow
	    let timeThen = page_timestamp[page]

	    return (timeThen > fakeTimeNow)
	}
    }
}
