/// tuhc-timemachine/mod.js --- Unlock Homestuck pages in real time

// Copyright (C) 2023  Bodertz

// Author: Bodertz <bodertz@gmail.com>

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/// Commentary:

// This is a mod for the Unoffical Homestuck Collection.  With it, one
// can travel back in time to when the comic was still updating.  Or
// at least pretend they have.  The mod takes a Unix timestamp and
// locks anything that's newer than that away.  As time passes, more
// and more content will unlock.

/// Known Issues:

// - There can be notifications that News posts have unlocked when in
//   actuality, this mod is still hiding them.  For example, visiting
//   page 008752 will show a notification for a full year's-worth of
//   news posts, but they won't be accessible without waiting.
//   Additionally, some news posts have incorrect timestamps, but this
//   is an issue from upstream.  See:
//   https://github.com/Bambosh/unofficial-homestuck-collection/issues/331.
//
// - Likely behaves strangely with New Reader Mode disabled.

/// Wishlist:

// - A timer showing when the next page will unlock would be nice.
//
// - Option to show date in titlebar using America/New_York timezone?
//
// - Add support for Ryanquest
//
// - Maybe simulate end of Gigapause crash
//
// - page 6386 eye fill, page 5414 blood colour

/// Code:

let store = null		// why?

const routes = require('./routes.js')
const jbTimestamps = require('./jb.timestamps.js')
const sbahjTimestamps = require('./sbahj.timestamps.js')
const sbahjPageIsSpoiler = sbahjTimestamps.sbahjPageIsSpoiler

module.exports = {
    title: "Time Machine",
    summary: "Pages unlock at the same rate they first released.",
    author: "Bodertz",
    modVersion: "0.7",

    description: `MSPA was not released all at once.  If you enjoy waiting a week (or a month (or a year)) for an update, this is the mod for you.  New Reader mode must be enabled.<br><br><b>To change settings, go here: <a href="/timemachine">Settings</a></b>`,

    settings: {
	boolean: [

	    {model: "dateInTitle",
	     label: "Show date in titlebar",
	     desc: "Check this if you want the Time Machine's current date to be displayed in the title bar."}

	    // {model: "exportState",
	    //  label: "Export current time settings",
	    //  desc: "Check to export to a file the number of seconds in the past the Time Machine is."}
	],

	radio: [
	    {
		model: "startAdventure",
		label: "Start Adventure",
		desc: `Selecting an adventure will travel back to the first page of that adventure.<br><br>

To instead input a custom date, go to <a href="/timemachine">/timemachine</a>.<br><br>

		In either case, you will need to enable New Reader Mode.`,
		options: [
		    {
			value: 1159156560, // Jailbreak timestamp
			label: "Jailbreak"
		    },
		    {
			value: 1181673278, // Bard Quest timestamp
			label: "Bard Quest"
		    },
		    {
			value: 1205126350, // Problem Sleuth timestamp
			label: "Problem Sleuth"
		    },
		    {
			value: 1239345528, // Homestuck Beta timestamp
			label: "Homestuck BETA"
		    },
		    {
			value: 1239607316, // Homestuck timestamp
			label: "Homestuck"
		    },
		    {
			value: "newReaderCurrent", // Placeholder string for value of $newReaderCurrent
			label: "New Reader Page",
			desc: "Choose this to travel back to the latest page from New Reader Mode."
		    }
	    ]}
	]
    },

    browserPages: {
	'TIMEMACHINE': {
	    component: {
		title: () => "Time Machine",
		template: `
		    <div class="pageBody">
			<h1>Time Machine</h1>
			<br><br>
			<form>
			    <div>
				<label>Travel to date and time:</label>
				<input type="datetime-local" id="dateSelector"/>

				<button type="button" onclick="travelToDate()">
				    Time Travel
				</button>
			    </div>
			</form>

			<br><br>

			<form>
			    <div>
				<label>Travel to timestamp (Unix Epoch):</label>
				<input type="number" id="timestampSelector"/>

				<button type="button" onclick="travelToTimestamp()">
				    Time Travel
				</button>
			    </div>
			</form>

			<br><br>

			<form>
			    <div>
				<label>Travel a custom number of seconds into the past:</label>
				<input type="number" id="secondsSelector"/>

				<button type="button" onclick="travelBackSeconds()">
				    Time Travel
				</button>
			    </div>
			</form>
		    </div>

		`,
		theme(ctx){
		    return 'retro'
		},
		scss: `& { background: #EEEEEE; }`
	    }
	}
    },

    computed(api) {
	store = api.store

	// Functions for /TIMEMACHINE
	travelToDate = function travelToDate() {
	    var dateSelector = document.querySelector("#dateSelector")
	    var date = new Date(dateSelector.value)

	    if (isNaN(date)) {
		alert(`The entered date is invalid!`)
	    } else {
		timeNow = Math.floor(new Date().getTime()/1000.0)
		timeThen = Math.floor(date.getTime()/1000.0)
		secondsFromThenToNow = timeNow - timeThen

		if (timeThen > timeNow)
		    return alert(`The entered date is in the future!`)

		store.set("secondsFromThenToNow", secondsFromThenToNow)
		alert(`Traveled to ${date.toDateString()}`)

		// Update status text on /TIMEMACHINE
		var currentStatus = document.querySelector("#currentStatus")
		currentStatus.innerHTML = `Seconds in the past: ${secondsFromThenToNow}`
	    }

	}

	    travelToTimestamp = function travelToTimestamp() {
	    var timestampSelector = document.querySelector("#timestampSelector")
	    var timestamp = timestampSelector.value

	    if (isNaN(timestamp) || timestamp < 0) {
		alert(`The entered timestamp is invalid!`)
	    } else {
		timeNow = Math.floor(new Date().getTime()/1000.0)
		timeThen = timestamp
		secondsFromThenToNow = timeNow - timeThen

		store.set("secondsFromThenToNow", secondsFromThenToNow)
		alert(`Traveled to ${new Date(timeThen * 1000.0).toDateString()}`)

		// Update status text on /TIMEMACHINE
		var currentStatus = document.querySelector("#currentStatus")
		currentStatus.innerHTML = `Seconds in the past: ${secondsFromThenToNow}`
	    }

	}

	travelBackSeconds = function travelBackSeconds() {
	    var secondsSelector = document.querySelector("#secondsSelector")
	    var seconds = secondsSelector.value

	    if (isNaN(seconds) || seconds <= 0) {
		alert(`The entered value is invalid!`)
	    } else {
		timeNow = Math.floor(new Date().getTime()/1000.0)
		timeThen = timeNow - seconds
		secondsFromThenToNow = seconds

		store.set("secondsFromThenToNow", secondsFromThenToNow)
		alert(`Traveled to ${new Date(timeThen * 1000.0).toDateString()}`)

		// Update status text on /TIMEMACHINE
		var currentStatus = document.querySelector("#currentStatus")
		currentStatus.innerHTML = `Seconds in the past: ${secondsFromThenToNow}`
	    }

	}



	store.set("startAdventure", store.get("startAdventure", false))
	startAdventure = store.get("startAdventure")

	store.set("dateInTitle", store.get("dateInTitle", true))
	dateInTitle = store.get("dateInTitle")

	if (startAdventure && startAdventure != "newReaderCurrent") {
	    // Selected option is start of adventure
	    let timeThen = startAdventure
	    let timeNow  = Math.floor(new Date().getTime()/1000.0)
	    store.set("secondsFromThenToNow", ((timeNow - timeThen) > 0) ? (timeNow - timeThen) : 0)
	    store.set("startAdventure", false) // Reset to false until user decides to travel again
	}

	//$pageIsSpoiler is much faster accessing this as a variable rather than with api.store.get
	secondsFromThenToNow = store.get("secondsFromThenToNow")
	startAdventure = store.get("startAdventure", false)

	console.log(`The time machine is currently ${secondsFromThenToNow} seconds in the past.`)

    },

    routes: routes.routes,

    edit(archive) {
	let pg = archive.mspa.story
	// Replace Jailbreak's timestamps with timestamps from forum threads
	jbTimestamps.jbTimestamps(archive)
	// Homestuck hidden pages
	pg['pony'].timestamp      = "1259628708" // timestamp for page 002838, where pony is linked from
	pg['darkcage'].timestamp  = "1324627030" // timestamp for page 006273, where darkcage is linked from
	pg['pony2'].timestamp     = "1330256453" // timestamp for page 006517, where pony2 is linked from
	pg['darkcage2'].timestamp = "1338946099" // timestamp for page 006927, where darkcage2 is linked from
    },

    vueHooks:[

	{match(c) {return true},	// always match (generally a bad idea, apparently)

	 methods: {
	     // Originally used only for newsposts
	     $timestampIsSpoiler(timestamp, $super) {
		 if (!this.$isNewReader) return false // from original method

		 if (secondsFromThenToNow > 0) {
		     let timeNow = Math.floor(new Date().getTime()/1000.0)
		     let fakeTimeNow = timeNow - secondsFromThenToNow
		     if (timestamp > fakeTimeNow) return true
		     else return false
		 }

		 // Call original method
		 return $super(timestamp)
	     },

	     //// Ensure pages are marked as spoilers
	     $pageIsSpoiler(page, useLimit, $super) {

		 // Work around subtle bug: if $pageIsSpoiler is
		 // called like $pageIsSpoiler(page), the method
		 // overriding machinery will place the original
		 // method in useLimit instead of $super.  If it's
		 // called like $pageIsSpoiler(page, [true|false]),
		 // the original method will correctly be placed in
		 // $super.
		 if (typeof(useLimit) == "function" && typeof($super) == "undefined") {
		     // Uh-oh!
		     // this.$logger.info(`Time Machine: useLimit is ${typeof(useLimit)}, $super is ${typeof($super)}.  Fixing...`)
		     $super = useLimit // Copy original method to $super
		     useLimit = false
		 }

		 if (!this.$archive) return true // Setup mode
		 pageString = page
		 parsedPage = parseInt(page)
		 if (parsedPage) {
		     pageString = String(parsedPage).padStart(6, '0')
		 } else if (page == 'jb2_000000') {
		     pageString = page
		 } else if (page.includes("sbahj")) {
		     return this.$isNewReader && sbahjPageIsSpoiler(page)
		 }

		 // this.$logger.info(`Page is ${page}, and I think the timestamp is ${this.$archive.mspa.story[page].timestamp}`)

		 let pageTimestamp = (typeof this.$archive.mspa.story[pageString] != "undefined") ? this.$archive.mspa.story[pageString].timestamp : false; // timestamp of page

		 if (this.$isNewReader && page in this.$archive.mspa.story) {
		     if (!!pageTimestamp && this.$timestampIsSpoiler(pageTimestamp))
			 return true

		     // Call original method
		     return $super(page, useLimit)
		 }

		 // Call original method
		 return $super(page, useLimit)

	     },

	     // Also hide albums
	     $albumIsSpoiler(ref, $super) {
		 if (this.$isNewReader && ref in this.$archive.music.albums && this.$archive.music.albums[ref].date) {
		     let albumTimestamp = new Date(this.$archive.music.albums[ref].date).getTime()/1000

		     return this.$timestampIsSpoiler(albumTimestamp)
		 }

		 // Call original method
		 return $super(ref)
	     }
	 }
	},
	// Also spoiler SBAHJ pages
	{matchName: "TabFrame",
	 computed: {
	     // Very, very gross, but need to copy the entirety of
	     // resolveComponent just to mark sbahj pages as spoilers

	     //// Copied code begins
	     resolveComponent() {
		 let mapRoute = {
                     'mspa': 'Page',
                     'jailbreak': 'Page',
                     'bard-quest': 'Page',
                     'problem-sleuth': 'Page',
                     'blood-spade': 'Page',
                     'beta': 'Page',
                     'homestuck': 'Page',
                     'story': 'Page',
                     'ryanquest': 'Page',
                     'waywardvagabond': 'ExtrasPage',
                     'sweetbroandhellajeff': 'SBAHJ',
                     'faqs': 'ExtrasPage',
                     'oilretcon': 'ExtrasPage',
                     'page': 'SinglePage',
                     'mspfa': 'MSPFADisambig',

		 }

		 const base = this.routeParams.base.toLowerCase()
		 let component = (base in mapRoute ? mapRoute[base] : base).toUpperCase()
		 if (!this.routeParams.base) component = 'Homepage'
		 switch (component) {
                 case 'PAGE': {
                     // Construct canonical story name and page number
                     let story_id
                     let page_num
                     const is_ryanquest = this.routeParams.base === 'ryanquest'
                     if (this.$isVizBase(this.routeParams.base)) {
                         const {s, p} = this.$vizToMspa(this.routeParams.base, this.routeParams.p)
                         story_id = s
                         page_num = p
                     } else {
                         page_num = this.routeParams.p
                         const tryLookup = this.$mspaToViz(page_num, is_ryanquest)
                         if (tryLookup) {
                             story_id = tryLookup.s
                         } else {
                             story_id = undefined // MSPA number does not map to valid viz story
                         }
                     }

                     // Lock ryanquest to mspa numbers for now
                     // if (is_ryanquest) {
                     //     page_num = this.$vizToMspa(this.routeParams.base, page_num).p
                     // }

                     const isTzPassword = !this.$archive
                         ? false
                         : (this.$archive.tweaks.tzPasswordPages.includes(page_num))

                     if (!(page_num && story_id)) component = 'Error404'
                    else if (this.$pageIsSpoiler(page_num, true) && !isTzPassword) component = 'Spoiler'
                    else if (
                        (story_id === 'ryanquest' && !(page_num in this.$archive.mspa.ryanquest)) ||
                        (story_id !== 'ryanquest' && !(page_num in this.$archive.mspa.story))
                    ) component = 'Error404'
                    else if (this.routeParams.base !== 'ryanquest') {
                        // If it's a new reader, take the opportunity to update the next allowed page for the reader to visit
                        this.$updateNewReader(page_num)

                        const flag = this.$archive.mspa.story[page_num].flag

                        if (flag.includes('X2COMBO')) component = 'x2Combo'
                        else if (flag.includes('FULLSCREEN') || flag.includes('DOTA') || flag.includes('GAMEOVER') || flag.includes('SHES8ACK')) component = 'fullscreenFlash'
                        else if (flag.includes('TZPASSWORD')) component = 'TzPassword'
                        else if (flag.includes('ECHIDNA')) component = 'Echidna'
                        else if (flag.includes('ENDOFHS')) component = 'EndOfHs'
                    }
                    break
                }
                case 'MUSIC': {
                    if (this.routeParams.mode == 'album') {
                        if (!(this.routeParams.id in this.$archive.music.albums)) component = 'Error404'
                        else if (this.$albumIsSpoiler(this.routeParams.id)) component = 'Spoiler'
                    }
                    else if (this.routeParams.mode == 'track') {
                        if (!(this.routeParams.id in this.$archive.music.tracks)) component = 'Error404'
                        else if (this.$trackIsSpoiler(this.routeParams.id)) component = 'Spoiler'
                    }
                    else if (this.routeParams.mode == 'artist') {
                        if (!(this.routeParams.id in this.$archive.music.artists)) component = 'Error404'
                    }
                    else if (this.routeParams.mode && !['tracks', 'artists', 'features'].includes(this.routeParams.mode)) component = 'Error404'
                    break
                }
                case 'SBAHJ': {
                    let num = parseInt(this.routeParams.cid)
                    if (!num || num < 0 || num > 54 || num == 39) component = 'Error404'

		    //// Copied code ends
		    else if (this.$pageIsSpoiler("sbahj" + num, true)) component = 'Spoiler'
		    return component.toUpperCase()
		    //// Copied code resumes
                    break
                }
                case 'PXS': {
                    if (this.$pageIsSpoiler('008753')) component = 'Spoiler'
                    // Paradox Space has its own internal 404 handler
                    // else if (this.routeParams.cid && !['archive', 'news', 'credits'].includes(this.routeParams.cid)) {
                    //     let p = parseInt(this.routeParams.pid) || 1
                    //     let data = this.$archive.comics.pxs.comics[this.routeParams.cid]
                    //     if (this.routeParams.cid && (!this.$archive.comics.pxs.list.includes(this.routeParams.cid) || !data || !Number.isInteger(p) || data.pages.length < p || p < 1)) component = 'Error404'
                    // }
                    break
                }
                case 'TSO': {
                    if (this.routeParams.cid) {
                        let p = parseInt(this.routeParams.pid)
                        let validComics = this.$archive.comics.tso.list.map(x => typeof(x) === 'object' ? x.list : x).flat()
                        let data = this.$archive.comics.tso.comics[this.routeParams.cid]
                        if (this.routeParams.cid && (!validComics.includes(this.routeParams.cid) || !data || !Number.isInteger(p) || data.pages.length < p || p < 1)) component = 'Error404'
                    }
                    break
                }
                case 'SNAPS': {
                    if (this.$isNewReader) component = 'Spoiler'
                    else if (this.routeParams.cid) {
                        let p = parseInt(this.routeParams.pid)
                        let data = this.$archive.comics.snaps.comics[this.routeParams.cid]
                        if (this.routeParams.cid && (!this.$archive.comics.snaps.list.includes(this.routeParams.cid) || !data || !Number.isInteger(p) || data.pages.length < p || p < 1)) component = 'Error404'
                    }
                    break
                }
                case 'SKAIANET': {
                    if (this.$isNewReader) component = 'Spoiler'
                    else if (this.routeParams.cursed_history && (this.routeParams.cursed_history != 'cursed_history' || !this.$localData.settings.cursedHistory)) component = 'Error404'
                    break
                }
                case 'SQUIDDLES': {
                    if (this.$pageIsSpoiler('004432')) component = 'Spoiler'
                    break
                }
                case 'UNLOCK': {
                    if (this.routeParams.p && this.routeParams.p.toLowerCase() === 'ps_titlescreen') component = 'PS_titlescreen'
                    else if (this.routeParams.p in this.$archive.mspa.psExtras) {
                        if ((this.routeParams.p == 'ps000039' && this.$pageIsSpoiler('003655')) || (this.routeParams.p == 'ps000040' && this.$pageIsSpoiler('003930'))) component = 'Spoiler'
                        else component = 'ExtrasPage'
                    }
                    else if (this.routeParams.p) component = 'Error404'
                    break
                }
                case 'EXTRASPAGE': {
                    let validBases = ['waywardvagabond', 'faqs', 'oilretcon']
                    if (validBases.includes(this.routeParams.base)) {
                        if (this.routeParams.base == 'waywardvagabond') {
                            let pages = {
                                recordsastutteringstep: '002148',
                                anunsealedtunnel: '002171',
                                anagitatedfinger: '002339',
                                astudiouseye: '002409',
                                beneaththegleam: '002623',
                                asentrywakens: '002744',
                                windsdownsideways: '002770',
                                preparesforcompany: '002921'
                            }
                            if (!(this.routeParams.p in pages)) component = 'Error404'
                            else if (this.$pageIsSpoiler(pages[this.routeParams.p])) component = 'Spoiler'
                        }
                        else if (this.routeParams.base == 'faqs' && !(this.routeParams.p in this.$archive.mspa.faqs)) component = 'Error404'
                        else if (this.routeParams.base == 'oilretcon' && this.$pageIsSpoiler('008993')) component = 'Spoiler'
                    }
                    else component = 'Error404'
                    break
                }
                case 'NAMCOHIGH': {
                    if (this.$pageIsSpoiler('008135')) component = 'Spoiler'
                    else if (this.routeParams.play && this.routeParams.play !== 'play') component = 'Error404'
                    break
                }
                case "DECODE":
                    if (this.routeParams.mode) {
                    if (!['morse','alternian','damaramegido'].includes(this.routeParams.mode)) component ='Error404'
                    if (
                        (this.routeParams.mode === 'alternian' && this.$pageIsSpoiler('003890')) ||
                        (this.routeParams.mode === 'damaramegido' && this.$pageIsSpoiler('007298'))
                    ) component = 'Spoiler'
                    }
                    break
                case 'BLOGSPOT': {
                    if (this.routeParams.id === 'magicaljourney') component = 'MagicalJourney'
                    else if (this.routeParams.id === 'offeryoucantrefuse') component = 'OfferYouCantRefuse'
                    else if (this.$pageIsSpoiler('002821')) component = 'Spoiler'
                    break
                }
                case 'DSTRIDER': {
                    if (this.$pageIsSpoiler('003053')) component = 'Spoiler'
                    break
                }
                case 'FORMSPRING': {
                    if (this.$pageIsSpoiler('003474')) component = 'Spoiler'
                    break
                }
                case 'TUMBLR': {
                    if (this.$pageIsSpoiler('006010')) component = 'Spoiler'
                    break
                }
                case "DESKTOPS": {
                    if (this.$pageIsSpoiler('003257')) component = 'Spoiler'
                    break
                }
                case "TBIY": {
                    if (this.$pageIsSpoiler('004527')) component = 'Spoiler'
                    break
                }
            }

            let result =
                (component.toUpperCase() in this.$options.components)
                || (component.toUpperCase() in this.modBrowserPages)
                ? component.toUpperCase()
                : 'ERROR404'
            return result
             }

	     //// Copied code ends
	 },
	 updated() {
	     // SBAHJ Newest Comic button
	     if (this.$isNewReader) {
		 const sbahjComics = ["sbahj1", "sbahj2", "sbahj3", "sbahj4", "sbahj5", "sbahj6", "sbahj7", "sbahj8", "sbahj9", "sbahj10",
				      "sbahj11", "sbahj12", "sbahj13", "sbahj14", "sbahj15", "sbahj16", "sbahj17", "sbahj18", "sbahj19", "sbahj20",
				      "sbahj21", "sbahj22", "sbahj23", "sbahj24", "sbahj25", "sbahj26", "sbahj27", "sbahj28", "sbahj29", "sbahj30",
				      "sbahj31", "sbahj32", "sbahj33", "sbahj34", "sbahj35", "sbahj36", "sbahj37", "sbahj38", "sbahj40",
				      "sbahj41", "sbahj42", "sbahj43", "sbahj44", "sbahj45", "sbahj46", "sbahj47", "sbahj48", "sbahj49", "sbahj50",
				      "sbahj51", "sbahj52", "sbahj53", "sbahj54"]

		 let latestComic
		 sbahjComics.reverse().some((comic) => {
		     // this.$logger.info(`${comic}`)
		     if (!this.$pageIsSpoiler(comic)) {
			 latestComic = comic.replace("sbahj", "/sbahj/")
			 // this.$logger.info(`latest comic is ${latestComic}`)
			 return true
		     }
		 })

		 // Replace "Newest Comic" link with link to latestComic
		 // There are two comicNavs, so the array is as follows:
		 // [first, previous, next, latest, first, previous, next, latest]
		 // We only care about latest and latest, which are [3] and [7].
		 let comicNavs = document.querySelectorAll('.comicNav a')
		 let comicNavsArray = Array.from(comicNavs)
		 let latestTop = comicNavsArray[3]
		 let latestBottom = comicNavsArray[7]
		 if (latestTop && latestBottom) {
		     latestTop.setAttribute('href', latestComic)
		     latestBottom.setAttribute('href', latestComic)
		 }


		 // Between 2014-09-08 and 2014-10-09, the ARFTER #46
		 // "link" was added.  Within that time, comic #51 was
		 // released, so I'll say the "link" was added along
		 // with that comic.
		 let arfter46 = document.querySelector('.linkhole div')
		 if (this.$pageIsSpoiler("sbahj51") && arfter46)
		     arfter46.remove()
	     }
	 }
	},
	// Hide SBAHJ linkhole links
	{matchName: "SweetBroAndHellaJeff",
	 data: {
	     listedPages($super) {
		 let pages = []
		 if (!this.$pageIsSpoiler("sbahj46")) pages.push({url: "/sbahj/46", title: "COMIC #46: the game........... is afoof"})
		 if (!this.$pageIsSpoiler("sbahj45")) pages.push({url: "/sbahj/45", title: "COMIC #45: cloink"})
		 if (!this.$pageIsSpoiler("sbahj44")) pages.push({url: "/sbahj/44", title: "COMIC #44: this is complete bulb shit"})
		 if (!this.$pageIsSpoiler("sbahj43")) pages.push({url: "/sbahj/43", title: "COMIC #433: BWEEEEEEEEEEEEEEEEEEEEE EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE EEEEEE EEEEEEEEEE EEEEEE EEEEEEEEEEEEEEEEEEEEEEEEEEE EEEEEEEEEEE EEE EEEEE"})
		 if (!this.$pageIsSpoiler("sbahj42")) pages.push({url: "/sbahj/42", title: "COMIC #42: shearching 4 bobby shitfuck"})
		 if (!this.$pageIsSpoiler("sbahj41")) pages.push({url: "/sbahj/41", title: "COMIC #41: IS THERE ANYTHING THAT DOISHE BLASTS THATS NOT QUADS."})
		 if (!this.$pageIsSpoiler("sbahj40")) pages.push({url: "/sbahj/40", title: "COMIC #40: bube just no"})
		 if (!this.$pageIsSpoiler("sbahj38")) pages.push({url: "/sbahj/38", title: "COMIC #38: pthhbhthbhpthhhthtbbbthb"})
		 if (!this.$pageIsSpoiler("sbahj37")) pages.push({url: "/sbahj/37", title: "COMIC #37: brough to u by the BIBTCHEZ &amp; THA GANJOG B)"})
		 if (!this.$pageIsSpoiler("sbahj36")) pages.push({url: "/sbahj/36", title: "COMIC #36: um, last I checked, the zoo didn't allow IDIDOT GAYS inside"})
		 if (!this.$pageIsSpoiler("sbahj35")) pages.push({url: "/sbahj/35", title: "COMIC #35: sububaway is for ONLY when your high, ONLY ... "})
		 if (!this.$pageIsSpoiler("sbahj34")) pages.push({url: "/sbahj/34", title: "COMIC #34: if this guy any more stoned then that, he's belong in the holy fucking buble"})
		 if (!this.$pageIsSpoiler("sbahj33")) pages.push({url: "/sbahj/33", title: "COMIC #33: BAOOOOOOOOOOOOOOOOOOOOOOOOOOAM!!!!!!!!!!!!!!!!"})
		 if (!this.$pageIsSpoiler("sbahj32")) pages.push({url: "/sbahj/32", title: "COMIC #32: (wall)"})
		 if (!this.$pageIsSpoiler("sbahj31")) pages.push({url: "/sbahj/31", title: "COMIC #31: tititled, \"THE GRODIRON\"!"})
		 if (!this.$pageIsSpoiler("sbahj30")) pages.push({url: "/sbahj/30", title: "COMIC #30: &lt;bold.....&gt;this redard can sit his soggy ass in a car on your shirt like an asshole FORE EVERiiiiiii"})
		 if (!this.$pageIsSpoiler("sbahj29")) pages.push({url: "/sbahj/29", title: "COMIC <b>#29</b>: NITE MANGIC!"})
		 if (!this.$pageIsSpoiler("sbahj28")) pages.push({url: "/sbahj/28", title: "COM<b>IC #28: spor</b>ts"})
		 if (!this.$pageIsSpoiler("sbahj27")) pages.push({url: "/sbahj/27", title: "COMIC #27: DONG"})
		 if (!this.$pageIsSpoiler("sbahj26")) pages.push({url: "/sbahj/26", title: "COMIC #26: this fapass dunkstick does this EVEREY fucking time"})
		 if (!this.$pageIsSpoiler("sbahj25")) pages.push({url: "/sbahj/25", title: "COMIC #25: more like legrend of the unreal shithead"})
		 if (!this.$pageIsSpoiler("sbahj24")) pages.push({url: "/sbahj/24", title: "COMIC #24: them man, the dickhead, the LERGEND."})
		 if (!this.$pageIsSpoiler("sbahj23")) pages.push({url: "/sbahj/23", title: "COMIC #23: oOoOoOoOoOoOoOoOooOoOoOOOoOOHHhhh hah just fucking with uou..."})
		 if (!this.$pageIsSpoiler("sbahj22")) pages.push({url: "/sbahj/22", title: "COMIC #22: borack bobdama STILL haven't got the hang of this ecolomy thing yet;"})
		 if (!this.$pageIsSpoiler("sbahj21")) pages.push({url: "/sbahj/21", title: "COMIC #21: this fine apparell secretly holds a mystery.... SHHHHHHHHHH"})
		 if (!this.$pageIsSpoiler("sbahj20")) pages.push({url: "/sbahj/20", title: "CO<b>MIC #20: CHECK OUT THUS HOT NEW MORC</b>H.........."})
		 if (!this.$pageIsSpoiler("sbahj19")) pages.push({url: "/sbahj/19", title: "COMIC #19: okay shup up everybody this shit is a SERIOUS, issue"})
		 if (!this.$pageIsSpoiler("sbahj18")) pages.push({url: "/sbahj/18", title: "COMIC #18: spoot problems? so want else is new........"})
		 if (!this.$pageIsSpoiler("sbahj17")) pages.push({url: "/sbahj/17", title: "COMIC #17: wowoof woof, aha hah what a fuckin IDIOT!"})
		 if (!this.$pageIsSpoiler("sbahjmovie1")) pages.push({url: "/sweetbroandhellajeff/movies/SBAHJthemovie1.swf", title: "MOVIE #1: hooly SHIT, wear a MOVIE???????????"})
		 if (!this.$pageIsSpoiler("sbahj16")) pages.push({url: "/sbahj/16", title: "COMIC #16: he LAUGHED when he shok, like a bowl full of WEED!!! omfg"})
		 if (!this.$pageIsSpoiler("sbahj15")) pages.push({url: "/sbahj/15", title: "COMIC #15: you don't boguard chips in the shitter dude, you just DONT"})
		 if (!this.$pageIsSpoiler("sbahj14")) pages.push({url: "/sbahj/14", title: "COMIC #14: HAHAHAHAHAHA they juts don't ever hit the ground, what is WRONG with this picture. \"3\" of ten"})
		 if (!this.$pageIsSpoiler("sbahj13")) pages.push({url: "/sbahj/13", title: "COMIC #13:um, EATH TO ASSHOLE. hello can you here me..... (part 2 of thenacho party thing)"})
		 if (!this.$pageIsSpoiler("sbahj12")) pages.push({url: "/sbahj/12", title: "COMIC #12: evrybody was high when this went down, EVERYBODY (part 1 of 10"})
		 if (!this.$pageIsSpoiler("sbahj11")) pages.push({url: "/sbahj/11", title: "COMIC #11: ok put the can down dude.... haha, unbealievible"})
		 if (!this.$pageIsSpoiler("sbahj10")) pages.push({url: "/sbahj/10", title: "COMIC #10: ok, AGAIN with the ANGEL problems. seariously dude??????????"})
		 if (!this.$pageIsSpoiler("sbahj9")) pages.push({url: "/sbahj/9", title: "COMIC #9: no way i mean no fucking way, NO WON is that good.."})
		 if (!this.$pageIsSpoiler("sbahj8")) pages.push({url: "/sbahj/8", title: "COMIC #8: just do me a FAVOR, put it bock in the fridge bro...."})
		 if (!this.$pageIsSpoiler("sbahj7")) pages.push({url: "/sbahj/7", title: "COMIC #7: a GAIN with the socks, you have GOT to be SHITTONG me"})
		 if (!this.$pageIsSpoiler("sbahj6")) pages.push({url: "/sbahj/6", title: "COMIC #6: this guy.... he OWNS the glass, he OWNS it"})
		 if (!this.$pageIsSpoiler("sbahj5")) pages.push({url: "/sbahj/5", title: "COMIC #5: sweet bro comes thru.......... AGAIN"})
		 if (!this.$pageIsSpoiler("sbahj4")) pages.push({url: "/sbahj/4", title: "COMIC #4: not even BARACK obana can bail him out of THIS jam....."})
		 if (!this.$pageIsSpoiler("sbahj3")) pages.push({url: "/sbahj/3", title: "COMIC #3: so messed up.... jelly??? omfg thats SO MESSED UP!!!"})
		 if (!this.$pageIsSpoiler("sbahj2")) pages.push({url: "/sbahj/2", title: "COMIC #2: HE HE HE"})
		 if (!this.$pageIsSpoiler("sbahj1")) pages.push({url: "/sbahj/1", title: "COMIC #1: man listen, stairs. i am TELLING you"})

		 return pages
		 }
	 },
	 computed: {
	     // Disable keyboard navigation to spoilered SBAHJ page
	     nextPage($super) {
		 let cid = parseInt(this.routeParams.cid)
		 let nextcid = (cid == 38) ? 40 :
		     (cid >= 54) ? 54 : cid + 1
		 // If next page is spoiler, set next page to current page
		 let nextpage = this.$pageIsSpoiler("sbahj" + nextcid) ? cid : nextcid

		 return nextpage
	     }
	 },
	    methods: {
		// A straight copy from the original, with the
		// exception of the last bit at the end:
		// Before: this.$pushURL(this.nextPage)
		// After: this.$pushURL("sbahj/" + this.nextPage)

		// For some reason, the links would go to, for
		// example, "/sbahj/23", but the next arrow would go
		// to "/23"
		keyNavEvent(dir, $super) {
		    if (dir == 'left' && this.$parent.$el.scrollLeft == 0) this.$pushURL(this.prevPage)
                    else if (dir == 'right' && this.$parent.$el.scrollLeft + this.$parent.$el.clientWidth == this.$parent.$el.scrollWidth) this.$pushURL("sbahj/" + this.nextPage)
		}
	    }
	},
	{matchName: "page",
	 methods: {
	     deretcon(media, $super) {
		 // Miscellaneous retcons, unrelated to main function of deretcon (media unused)

		 // 2009
		 const originalContent001901 = this.$archive.mspa.story['001901'].content.replace("13th of April, 2009, is this", "13th of April, is this")
		 this.$isNewReader && this.$timestampIsSpoiler('1413490560') // Timestamp for end of Gigapause, as the 2009 edit was made around then
		     ? this.$archive.mspa.story['001901'].content = originalContent001901
		     : this.$archive.mspa.story['001901'].content = originalContent001901.replace("13th of April, is this", "13th of April, 2009, is this")
		 // Calliope
		 this.$isNewReader && this.$timestampIsSpoiler('1340033902') // page release + 12 hours (1339990702 + 43200), estimate, max is about 17 hours
		     ? this.$archive.mspa.story['006997'].content = `
<p style=" font-weight: bold; font-family: courier, monospace;color:#000000">
[Author's note:<br><br>Let's avoid posting spoiler images all over tumblr, just this once?<br>
<span style="white-space: nowrap;"><br>If you absolutely MUST proliferate an image, <a href="/storyfiles/hs2/scraps/calliope.gif" target="_blank" class="postlink">please post this instead</a>.]</span></p>
`
		     : this.$archive.mspa.story['006997'].content = ""

		 // Peachy
		 let unpeachy_p = this.$localData.settings['unpeachy']
		 if (this.$isNewReader && this.$timestampIsSpoiler('1358114400')) { // January 13 2013 5:00pm (tumblr post)
		     // Before peachy
		     this.$archive.mspa.story['007623'].media[1] = this.$archive.mspa.story['007623'].media[1].replace('scraps/fruitone', '05720_2')
		     this.$archive.mspa.story['007623'].content = this.$archive.mspa.story['007623'].content.replace('PEACHY.gif', 'CAUCASIAN.gif')
		 } else {
		     // After peachy
		     if (!unpeachy_p) { // Respect official settings
			 this.$archive.mspa.story['007623'].media[1] = this.$archive.mspa.story['007623'].media[1].replace('05720_2', 'scraps/fruitone')
			 this.$archive.mspa.story['007623'].content = this.$archive.mspa.story['007623'].content.replace('CAUCASIAN.gif', 'PEACHY.gif')
		     }
		 }


		 // TODO

		 // WELCOME BACK
		 // TODO(?) Crash all other pages?
		 if (this.$isNewReader && this.$timestampIsSpoiler('1413490800')) { // Site crashed at this point
		     // Before crash
		     this.$archive.mspa.story['008753'].content = "WELCOME BACK."
		     this.$archive.mspa.story['008753'].timestamp = 1413490560 // https://mspaintadventureswiki.tumblr.com/post/100186107568/exacerbating-the-already-unwieldy-hype-is-the-last
		 } else {
		     // After crash
		     this.$archive.mspa.story['008753'].content = "WELCOME BACK.<br><br>(AGAIN.)"
		     this.$archive.mspa.story['008753'].timestamp = 1413585560 // From mspa.json
		 }
		 // Íaeû ë€Å
		 const originalContent002286 = this.$archive.mspa.story['002286'].content
		 if (this.$isNewReader && this.$timestampIsSpoiler('1326672000')) { // January 16, 2012 - just a guess
		     // Before edit
		     this.$archive.mspa.story['002286'].content = originalContent002286
		 } else if (this.$isNewReader && this.$timestampIsSpoiler('1327266900')) { // January 22, 2012 - just a guess
		     // After edit
		     this.$archive.mspa.story['002286'].content = originalContent002286.replace("being a white guy who is a rapper", "being a Íæûë€Å guy who is a rapper")
		 } else {
		     // After when it changed back
		     this.$archive.mspa.story['002286'].content = originalContent002286
		 }

		 // Call original method
		 return $super(media)

	     }
	 },
	 computed: {
	     // If the next page is a spoiler, it doesn't exist yet!  Hide it!
	     // TODO: Make use of $super.  Things aren't working right when I try.
	     nextPagesArray($super) {
		 let nextPages = []
		 this.thisPage.next.forEach(nextID => {
		     if (!this.$shouldRetcon('retcon6') && this.retcon6passwordPages.includes(nextID)) return // From original method
		     if (this.$pageIsSpoiler(nextID, true)) return // useLimit set to true  TODO: make sure there are no issues with character selects
		     nextPages.push(this.pageCollection[nextID.trim()])
		 })
		 return nextPages
	     },
	     pageMedia($super) {

		 const media = Array.from(this.thisPage.media) // From original method
		 this.deretcon(media)			       // From original method
		 var mediakey = media[0]		       // From original method

		 // Character Select Screens + Jailbreak loop

		 // Originally, page 006000 looped back to the start
		 // of Jailbreak until page 006001 was released later.

		 // Modified flash files which display "panel is not
		 // done yet" are provided to replace the flash files
		 // in the collection.  These flash files replace the
		 // original when the page(s) the flash file links to
		 // is/are not ye unlocked.  As the pages unlock,
		 // different versions of the flash file are swapped
		 // in which link to the now unlocked pages.  When all
		 // pages the flash links to are unlocked, the
		 // original flash file from TUHC is shown.
		 const flashFiles = [
		     // Jailbreak loop
		     "04097.swf",
		     // Character Selects
		     "04118.swf", "04159.swf", "04466.swf", "04513.swf", "05031.swf",
		     "05049.swf", "05067.swf", "05075.swf", "05134.swf", "26297.swf", "07482.swf"
		 ]
		 if (flashFiles.some(flashFile => mediakey.includes(flashFile))) {
		     let baseName = mediakey.substring(mediakey.length - 9, mediakey.length) // 5-digit number + '.swf'
		     let flashPath = mediakey.substring(0, mediakey.length - 4) // chop off '.swf'

		     // this.$pageIsSpoiler accepts an optional second
		     // argument, useLimit.  We set this to true here
		     // so that pages the character select screens
		     // link to are not considered to be spoilers
		     // (unless we're too far in the past, of course,
		     // but useLimit doesn't know anything about
		     // that).
		     switch (baseName) {
		     // Jailbreak loop
		     case "04097.swf":
			 if (this.$pageIsSpoiler("006001", true))          // timestamp 1319517911
			     media[0] = `${flashPath}_original.swf`
			 break;

		     // Character Selects
		     case "04118.swf":
		     case "04159.swf":
			 if (this.$pageIsSpoiler("006022", true)) {        // timestamp 1321065129
			     media[0] = `${flashPath}_unfinished1.swf`
			 } else if (this.$pageIsSpoiler("006063", true)) { // timestamp 1321586886
			     media[0] = `${flashPath}_unfinished2.swf`
			 }
			 break;
		     case "04466.swf":
		     case "04513.swf":
			 if (this.$pageIsSpoiler("006370", true)) {        // timestamp 1326834210
			     media[0] =  `${flashPath}_unfinished1.swf`
			 } else if (this.$pageIsSpoiler("006417", true)) { // timestamp 1327970898
			     media[0] =  `${flashPath}_unfinished2.swf`
			 }
			 break;
		     case "05031.swf":
		     case "05049.swf":
		     case "05067.swf":
		     case "05075.swf":
			 if (this.$pageIsSpoiler("006935", true)) {        // timestamp 1339235955
			     media[0] = `${flashPath}_unfinished1.swf`
			 } else if (this.$pageIsSpoiler("006953", true)) { // timestamp 1339379483
			     media[0] = `${flashPath}_unfinished2.swf`
			 } else if (this.$pageIsSpoiler("006971", true)) { // timestamp 1339554347
			     media[0] = `${flashPath}_unfinished3.swf`
			 } else if (this.$pageIsSpoiler("006979", true)) { // timestamp 1339661806
			     media[0] = `${flashPath}_unfinished4.swf`
			 }
			 break;
		     case "05134.swf":
			 if (this.$pageIsSpoiler("007038", true)) {        // timestamp 1340698863
			     media[0] = `${flashPath}_unfinished1.swf`
			 } else if (this.$pageIsSpoiler("007051", true)) { // timestamp 1340699348
			     media[0] = `${flashPath}_unfinished2.swf`
			 } else if (this.$pageIsSpoiler("007076", true)) { // timestamp 1340700195
			     media[0] = `${flashPath}_unfinished3.swf`
			 } else if (this.$pageIsSpoiler("007087", true)) { // timestamp 1340701139
			     media[0] = `${flashPath}_unfinished4.swf`
			 }
			 break;
		     case "26297.swf":
			 if (this.$pageIsSpoiler("008200", true)) {        // timestamp 1371627287
			     media[0] = `${flashPath}_unfinished1.swf`
			 } else if (this.$pageIsSpoiler("008224", true)) { // timestamp 1372620344
			     media[0] = `${flashPath}_unfinished2.swf`
			 } else if (this.$pageIsSpoiler("008243", true)) { // timestamp 1373516520
			     media[0] = `${flashPath}_unfinished3.swf`
			 }
			 break;
		     case "07482.swf":
			 if (this.$pageIsSpoiler("009393", true)) {        // timestamp 1430687943
			     media[0] = `${flashPath}_unfinished2.swf`
			 } else if (this.$pageIsSpoiler("009397", true)) { // timestamp 1430782118
			     media[0] = `${flashPath}_unfinished3.swf`
			 } else if (this.$pageIsSpoiler("009400", true)) { // timestamp 1430864924
			     media[0] = `${flashPath}_unfinished4.swf`
			 } else if (this.$pageIsSpoiler("009402", true)) { // timestamp 1430954892
			     media[0] = `${flashPath}_unfinished5.swf`
			 } else if (this.$pageIsSpoiler("009408", true)) { // timestamp 1431035620
			     media[0] = `${flashPath}_unfinished6.swf`
			 } else if (this.$pageIsSpoiler("009410", true)) { // timestamp 1431298976
			     media[0] = `${flashPath}_unfinished7.swf`
			 } else if (this.$pageIsSpoiler("009413", true)) { // timestamp 1431385772
			     media[0] = `${flashPath}_unfinished8.swf`
			 }
			 break;
		     }
		     return media
		 }
		 // Call original method
		 // return $super()
		 return media
	     }
	 }
	},
	// Add Fake Date to Titlebar
	// Also $newReaderCurrent hack
	{matchName: "titleBar",
	 computed: {
	     activeTabTitle($super) {

		 //// Start of $newReaderCurrent Hack

		 // The modding api doesn't allow accessing
		 // $newReaderCurrent when we need to, so we access it
		 // here instead.  Hacky, but what can you do.
		 if (startAdventure == "newReaderCurrent") {
		     if (!this.$isNewReader) {
			 store.set("startAdventure", false)
			 store.set("secondsFromThenToNow", 0)
		     } else {
			 // startAdventure is a string placeholder, so we
			 // fill that and bring things to a sensible state
			 let timeNow = Math.floor(new Date().getTime()/1000.0)
			 let newReaderPage = this.$archive.mspa.story[this.$newReaderCurrent]
			 let timeThen = newReaderPage.timestamp
			 let newSecondsFromThenToNow = timeNow - timeThen

			 this.$logger.info(`Time Machine: Replacing placeholder newReaderCurrent with timestamp ${timeThen} from page ${newReaderPage.pageId}.`)
			 store.set("secondsFromThenToNow", newSecondsFromThenToNow)
			 secondsFromThenToNow = store.get("secondsFromThenToNow")
			 store.set("startAdventure", false) // Don't go back here every time
			 startAdventure = false
		     }
		 }

		 //// End of $newReaderCurrent Hack

		 // if (typeof secondsFromThenToNow != "undefined") { ... }
		 if (dateInTitle) {
		     let baseTitle = this.$localData.tabData.tabs[this.activeTabKey].title
		     let timeNow = Math.floor(new Date().getTime()/1000.0)
		     let fakeTimeNow = timeNow - secondsFromThenToNow
		     const options = {year: 'numeric', month: 'short', day: 'numeric' }
		     let fakeTimeNowFormatted = new Date(fakeTimeNow * 1000).toLocaleDateString(undefined, options)

		     if (this.$archive && fakeTimeNow != timeNow)
			 return baseTitle + " [Time Machine: " + fakeTimeNowFormatted + "]"
		     else return baseTitle
		 }

		 // Call original method
		 return $super()
	     }
	 }
	}
    ]
}
